# SEEK Ads - API

> SEEK Ads - API

## Build Setup
> Assume already had postgresql db installed locally
> CREATE DATABASE seek;
> Assume already installed sequelize-cli or npm install -g sequelize-cli
> RUN DB MIGRATION USING >> sequelize db:migrate

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm run dev
```

### Documentation
Backend Structure consist of Hapi.js & Sequelize > PostgreSql

- route folder -> control all the route of API
- server folder -> split by {
    config: sequelize config to connect to db
    controller: this is core controller of API
    migrations: sequelize migration file
    seeder: not used - to create dummy data
}
