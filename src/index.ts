'use strict';

import * as hapi from "hapi";
import router from './router/Router';

const server: hapi.Server = new hapi.Server();
server.connection({ port: 3000, host: 'localhost', 
    routes: {
        cors: true
    }
});

// .register(...) registers a module within the instance of the API. The callback is then used to tell that the loaded module will be used as an authentication strategy. 
server.register([require('bell')], ( err ) => {

    router.forEach( ( router ) => {
        server.route( router );
    } );

} );

server.start((err) => {

    if (err) {
        throw err;
    }
    console.log(`Server running at: ${server.info.uri}`);
});