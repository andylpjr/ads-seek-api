'use strict';

import models from './server/models/index';

const uuid = require('uuid4');
const bcrypt = require('bcrypt');
const crypto = require("crypto");

const sequelize = models.sequelize;

// Generate UUID V4
export function generateUuid(model) {
    let uniqueId = uuid();

    if(uuid.valid(uniqueId)) {
        model.findAll({
            where: {
                id: uniqueId
            }
        })
        .then(model => model)
        .catch(error => error)
    }

    return uniqueId;
}

// Check Valid UUID V4 String
export async function checkUuid(id: string) {
    return (await uuid.valid(id) === true) ? true : false;
}

// check empty object
export function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// run sequelize raw query
export async function rawQuery(query: string) {
    return sequelize.query(query, { type: sequelize.QueryTypes.SELECT})
    .then(result => {
        return result;
    });
}

// sum total of checkout
export function sumCheckoutDetail(detail, origin) {
    if(origin) {
        return detail.reduce((accumulator, current) => accumulator + (current.qty * current.price), 0);
    } else {
        return detail.reduce((accumulator, current) => accumulator + current.finalPrice, 0);
    }
}

// generate Invoice number
export function generateInvoice() {
    const invoiceNo = crypto.randomBytes(4).toString('hex').toString().toUpperCase()

    return `INV/${invoiceNo}`;
}