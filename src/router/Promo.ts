import controllers from '../server/controllers/Index'

const Promo = [
    {
        method: 'GET',
        path: '/promos',
        handler: controllers.Promos.default.list
    },
    {
        method: 'GET',
        path: '/promos/detail',
        handler: controllers.Promos.default.listById
    },
    {
        method: 'POST',
        path: '/promos/create',
        handler: controllers.Promos.default.create
    },
    {
        method: 'POST',
        path: '/promos/update',
        handler: controllers.Promos.default.update
    },
    {
        method: 'POST',
        path: '/promos/delete',
        handler: controllers.Promos.default.delete
    },
    {
        method: 'GET',
        path: '/promos/search',
        handler: controllers.Promos.default.searchPromos
    },
]

export default Promo