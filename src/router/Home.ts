const Home = [
    {
        method: 'GET',
        path: '/',
        config: {
            handler: function(request, reply) {
                return reply({
                    status: 'success',
                    message: 'Welcome to SEEK Server',
                    data: {}
                });
            }
        }
    }
]

export default Home