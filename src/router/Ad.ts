import controllers from '../server/controllers/Index'

const Ad = [
    {
        method: 'GET',
        path: '/ads',
        handler: controllers.Ads.default.list
    },
    {
        method: 'GET',
        path: '/ads/detail',
        handler: controllers.Ads.default.listById
    },
    {
        method: 'POST',
        path: '/ads/create',
        handler: controllers.Ads.default.create
    },
    {
        method: 'POST',
        path: '/ads/update',
        handler: controllers.Ads.default.update
    },
    {
        method: 'POST',
        path: '/ads/delete',
        handler: controllers.Ads.default.delete
    },
    {
        method: 'GET',
        path: '/ads/search',
        handler: controllers.Ads.default.searchAds
    },
]

export default Ad