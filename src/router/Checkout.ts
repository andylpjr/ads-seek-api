import controllers from '../server/controllers/Index'

const Checkout = [
    {
        method: 'GET',
        path: '/checkouts',
        handler: controllers.Checkouts.default.list
    },
    {
        method: 'GET',
        path: '/checkouts/detail',
        handler: controllers.Checkouts.default.listById
    },
    {
        method: 'POST',
        path: '/checkouts/generate',
        handler: controllers.Checkouts.default.generate
    },
    {
        method: 'POST',
        path: '/checkouts/create',
        handler: controllers.Checkouts.default.create
    },
]

export default Checkout