'use strict';

import Home from './Home'
import Ad from './Ad'
import Promo from './Promo'
import Customer from './Customer'
import Checkout from './Checkout'

let router;
// Import all route
router = [
    Home,
    Ad,
    Promo,
    Customer,
    Checkout
];

export default router