import controllers from '../server/controllers/Index'

const Customer = [
    {
        method: 'GET',
        path: '/customers',
        handler: controllers.Customers.default.list
    },
    {
        method: 'GET',
        path: '/customers/detail',
        handler: controllers.Customers.default.listById
    },
    {
        method: 'POST',
        path: '/customers/create',
        handler: controllers.Customers.default.create
    },
    {
        method: 'POST',
        path: '/customers/update',
        handler: controllers.Customers.default.update
    },
    {
        method: 'POST',
        path: '/customers/delete',
        handler: controllers.Customers.default.delete
    },
    {
        method: 'GET',
        path: '/customers/search',
        handler: controllers.Customers.default.searchCustomers
    },
]

export default Customer