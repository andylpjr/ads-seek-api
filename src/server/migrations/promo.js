'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Promo', {
      id: {
        allowNull: false,
        type: Sequelize.UUID,
        primaryKey: true
      },
      code: {
        type: Sequelize.STRING,
      },
      type: {
        type: Sequelize.STRING,
      },
      adId: {
        type: Sequelize.UUID
      },
      qty: {
        type: Sequelize.INTEGER
      },
      freeQty: {
        type: Sequelize.INTEGER
      },
      discount: {
        type: Sequelize.DECIMAL(16, 4)
      },
      price: {
        type: Sequelize.DECIMAL(16, 4)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Promo');
  }
};