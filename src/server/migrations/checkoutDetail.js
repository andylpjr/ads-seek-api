'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CheckoutDetail', {
      id: {
        allowNull: false,
        type: Sequelize.UUID,
        primaryKey: true
      },
      checkoutId: {
        type: Sequelize.UUID
      },
      adId: {
        type: Sequelize.UUID
      },
      qty: {
        type: Sequelize.INTEGER
      },
      price: {
        type: Sequelize.DECIMAL(16, 4)
      },
      promoId: {
        type: Sequelize.UUID
      },
      finalPrice: {
        type: Sequelize.DECIMAL(16, 4)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CheckoutDetail');
  }
};