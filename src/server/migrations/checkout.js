'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Checkout', {
      id: {
        allowNull: false,
        type: Sequelize.UUID,
        primaryKey: true
      },
      documentNo: {
        type: Sequelize.STRING,
      },
      date: {
        type: Sequelize.DATEONLY
      },
      customerId: {
        type: Sequelize.UUID
      },
      amount: {
        type: Sequelize.DECIMAL(16, 4)
      },
      finalAmount: {
        type: Sequelize.DECIMAL(16, 4)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Checkout');
  }
};