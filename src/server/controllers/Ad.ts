import models from '../models/index';
import { generateUuid } from '../../Helper';

const Ad = models.Ad;
const Op = models.Sequelize.Op;

export default {
    // Retrieve all Ad
    list(req, res) {
        return Ad
        .findAll({
            attributes: ['id', 'code', 'name', 'price'],
        })
        .then(ad => res({
            data: ad
        }).code(200))
        .catch(error => res(error).code(400));
    },
    // Retrieve all Ad
    listById(req, res) {
        return Ad
        .find({
            attributes: ['id', 'code', 'name', 'price'],
            where: {
                id: req.query.adId
            }
        })
        .then(ad => res({
            data: ad
        }).code(200))
        .catch(error => res(error).code(400));
    },
    // Create Ad
    create(req, res) {
        const ad = {
            id: generateUuid(Ad),
            code: req.payload.code,
            name: req.payload.name,
            price: req.payload.price,
            createdAt: new Date().toString(),
            updatedAt: new Date().toString()
        };
        return models.sequelize.transaction(function (t) {       
            return Ad.create(ad, {transaction: t})          
        })
        .then(() => {
            res({
                message: "Congratulations, Your Ad has been created!",
                data: {}
            }).code(201);
        }).catch(err => {
            res({
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    update(req, res) {
        const updatedAd = {
            code: req.payload.code,
            name: req.payload.name,
            price: req.payload.price,
            updatedAt: new Date().toString()
        }      
        return models.sequelize.transaction(function (t) {
            return Ad.find({
                where: {
                    id: req.payload.id
                }
            })
            .then(ad => {
                // Check if record exists in db
                if (ad) {
                    ad.update(updatedAd)
                } else {
                    res({
                        message: 'Ad does not exist',
                        data: {}
                    }).code(400);
                }
            });
            
        })
        .then(() => {
            res({
                message: 'You have successful update your ad',
                data: {}
            }).code(200);
        }).catch(err => {
            res({
                status: 'error',
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    delete(req, res) {
        return models.sequelize.transaction(function (t) {
            return Ad.destroy({where: {id: req.payload.id}, transaction: t})            
        })
        .then(() => {
            res({
                message: "Ad has been deleted!",
                data: {}
            }).code(200);
        }).catch(err => {
            res({
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    // Retrieve Ads By Searching
    searchAds(req, res) {
        
        let { key } = req.query;
        let offset = 0;
        let pages = 0;
        let dataCount = 0;
        key = (key === '') ? key : `%${key}%`

        return Ad.findAll({
            attributes: ['id', 'code', 'name'],
            where: {
                code: {
                    [Op.iLike]: (key === '') ? '' : key
                }
            }
        })
        .then(ad => res({
            message: '',
            data: ad
        }).code(200))
        .catch(error => res({
            message: error.toString(),
            data: {}
        }).code(400))
    },
};

