import models from '../models/index';
import { generateUuid, sumCheckoutDetail, generateInvoice } from '../../Helper';

const Checkout = models.Checkout;
const CheckoutDetail = models.CheckoutDetail;
const Customer = models.Customer;
const CustomerPromo = models.CustomerPromo;
const Promo = models.Promo;
const Ad = models.Ad;
const Op = models.Sequelize.Op;

export default {
    // Retrieve all Checkout
    list(req, res) {
        return Checkout
        .findAll({
            attributes: ['id', 'documentNo', 'date', 'customerId', 'amount', 'finalAmount'],
            include: [
                {
                    attributes: ['code', 'name'],
                    model: Customer
                }
            ]
        })
        .then(checkout => {
            // restructure checkout response to make client easy read
            const finalCheckout = checkout.map(x => ({
                id: x.id,
                documentNo: x.documentNo,
                date: x.date,
                customerId: x.customerId,
                customerName: x.Customer.name,
                amount: x.amount,
                finalAmount: x.finalAmount
            }))
            res({
                data: finalCheckout
            }).code(200)
        })
        .catch(error => res(error).code(400));
    },
    // Retrieve promo by id
    listById(req, res) {
        return Checkout
        .find({
            attributes: ['id', 'documentNo', 'date', 'customerId', 'amount', 'finalAmount'],
            where: {
                id: req.query.checkoutId
            },
            include: [
                {
                    attributes: ['code', 'name'],
                    model: Customer
                },
                {
                    attributes: ['id', 'checkoutId', 'adId', 'qty', 'price', 'promoId', 'finalPrice'],
                    model: CheckoutDetail,
                    include: [
                        {
                            model: Ad
                        },
                        {
                            model: Promo
                        }
                    ]
                }
            ]
        })
        .then(checkout => {
            // restructure checkout response to make client easy read
            const finalCheckout = {
                id: checkout.id,
                documentNo: checkout.documentNo,
                date: checkout.date,
                customerId: checkout.customerId,
                customerCode: checkout.Customer.code,
                customerName: checkout.Customer.name,
                amount: checkout.amount,
                finalAmount: checkout.finalAmount,
                detail: checkout.CheckoutDetails.map(x => ({
                    id: x.id,
                    checkoutId: x.id,
                    adId: x.adId,
                    adName: x.Ad.name,
                    qty: x.qty,
                    price: x.price,
                    promoId: (x.promoId === null ? '' : x.promoId),
                    promoCode: (x.promoId === null ? '' : x.Promo.code),
                    finalPrice: x.finalPrice
                }))
            }
            res({
                data: finalCheckout
            }).code(200)
        })
        .catch(error => res(error).code(400));
    },
    // generate Checkout data for confirmation
    async generate(req, res) {
        const originAmount = sumCheckoutDetail(req.payload.detail, true);
        const customerPromo = await CustomerPromo.findAll({
            where: {
                customerId: req.payload.customerId    
            },
            include: [
                {
                    model: Promo,
                }
            ]
        })
        // If customer have promo
        if(customerPromo.length > 0) {
            const listPromo = customerPromo.map(promo => ({
                promoId: promo.promoId,
                promoCode: promo.Promo.code,
                type: promo.Promo.type,
                adId: promo.Promo.adId,
                qty: promo.Promo.qty,
                freeQty: promo.Promo.freeQty,
                discount: promo.Promo.discount,
                price: promo.Promo.price
            }))
            const detail = req.payload.detail.map(x => {
                const filteredPromo = listPromo.filter(promo => promo.adId === x.adId)
                if(filteredPromo.length > 0) {
                    // If customer promo for this ad is Free Qty Rule
                    if(filteredPromo[0].type === 'Free') {
                        let qtyToPay = 0;
                        const qtyRule = filteredPromo[0].qty + filteredPromo[0].freeQty;
                        
                        // CASE EXAMPLE : BUY 4 GET 1, BOUGHT 5 MUST PAY 4
                        if(qtyRule - x.qty === 0) {
                            qtyToPay = filteredPromo[0].qty
                        }
                        // CASE EXAMPLE : BUY 4 GET 1, BOUGHT 6 MUST PAY 5
                        if(qtyRule - x.qty < 0) {
                            qtyToPay = qtyRule
                        }
                        // CASE EXAMPLE : BUY 4 GET 1, BOUGHT 3 MUST PAY 3
                        if(qtyRule - x.qty > 0) {
                            qtyToPay = x.qty
                        }
                        
                        return {
                            adId: x.adId,
                            adName: x.adName,
                            qty: x.qty,
                            price: x.price,
                            promoId: filteredPromo[0].promoId,
                            promoCode: filteredPromo[0].promoCode,
                            finalPrice: qtyToPay * x.price
                        }
                    } else {
                        // If customer promo for this Ad is Discount Price Rule   
                        const qtyRule = filteredPromo[0].qty;
                        const priceRule = filteredPromo[0].price;
                        const priceToPay = (x.qty - qtyRule >= 0) ? priceRule : x.price;
                        return {
                            adId: x.adId,
                            adName: x.adName,
                            qty: x.qty,
                            price: x.price,
                            promoId: filteredPromo[0].promoId,
                            promoCode: filteredPromo[0].promoCode,
                            finalPrice: x.qty * priceToPay
                        }
                    }
                } else {
                    // If customer's promo not for this Ad Id
                    return {
                        adId: x.adId,
                        adName: x.adName,
                        qty: x.qty,
                        price: x.price,
                        promoId: '',
                        promoCode: '',
                        finalPrice: x.qty * x.price
                    }
                }
            })
            const totalAmount = sumCheckoutDetail(detail, false);
            const checkoutData = {
                date: new Date().toString(),
                customerId: req.payload.customerId,
                customerCode: req.payload.customerCode,
                customerName: req.payload.customerName,
                amount: originAmount,
                finalAmount: totalAmount,
                detail: detail
            }
            res({
                data: checkoutData
            }).code(200);
        } else {
            // If customer don't have promo just plain sum qty * price
            const detail = req.payload.detail.map(x => ({
                adId: x.adId,
                adName: x.adName,
                qty: x.qty,
                price: x.price,
                promoId: '',
                promoCode: '',
                finalPrice: x.qty * x.price
            }))
            const totalAmount = sumCheckoutDetail(detail, false);
            const checkoutData = {
                date: new Date().toString(),
                customerId: req.payload.customerId,
                customerCode: req.payload.customerCode,
                customerName: req.payload.customerName,
                amount: originAmount,
                finalAmount: totalAmount,
                detail: detail
            }
            res({
                data: checkoutData
            }).code(200);
        }
    },
    // Create Checkout
    create(req, res) {
        const checkout = {
            id: generateUuid(Checkout),
            documentNo: generateInvoice(),
            date: new Date().toString(),
            customerId: req.payload.customerId,
            amount: req.payload.amount,
            finalAmount: req.payload.finalAmount,
            createdAt: new Date().toString(),
            updatedAt: new Date().toString()
        };
        const checkoutDetail = req.payload.detail.map(x => ({
            id: generateUuid(CheckoutDetail),
            checkoutId: checkout.id,
            adId: x.adId,
            qty: x.qty,
            price: x.price,
            promoId: (x.promoId === '' ? null : x.promoId),
            finalPrice: x.finalPrice
        }))
        return models.sequelize.transaction(function (t) {       
            return Checkout.create(checkout, {transaction: t})
            .then(() => {
                return CheckoutDetail.bulkCreate(checkoutDetail, {transaction: t})
            })        
        })
        .then(() => {
            res({
                message: "Congratulations, Your Checkout has been created!",
                data: {}
            }).code(201);
        }).catch(err => {
            res({
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
};

