import models from '../models/index';
import { generateUuid } from '../../Helper';

const Promo = models.Promo;
const Ad = models.Ad;
const Op = models.Sequelize.Op;

export default {
    // Retrieve all Promo
    list(req, res) {
        return Promo
        .findAll({
            attributes: ['id', 'code', 'type', 'adId', 'qty', 'freeQty', 'discount', 'price'],
            include: [
                {
                    attributes: ['code', 'name'],
                    model: Ad
                }
            ]
        })
        .then(promo => {
            // restructure promo response to make client easy read
            const finalPromo = promo.map(x => ({
                id: x.id,
                code: x.code,
                type: x.type,
                adId: x.adId,
                adCode: x.Ad.code,
                adName: x.Ad.name,
                qty: x.qty,
                freeQty: x.freeQty,
                discount: x.discount,
                price: x.price
            }))
            res({
                data: finalPromo
            }).code(200)
        })
        .catch(error => res(error).code(400));
    },
    // Retrieve promo by id
    listById(req, res) {
        return Promo
        .find({
            attributes: ['id', 'code', 'type', 'adId', 'qty', 'freeQty', 'discount', 'price'],
            where: {
                id: req.query.promoId
            },
            include: [
                {
                    attributes: ['code', 'name'],
                    model: Ad
                }
            ]
        })
        .then(Promo => res({
            data: Promo
        }).code(200))
        .catch(error => res(error).code(400));
    },
    // Create Promo
    create(req, res) {
        const promo = {
            id: generateUuid(Promo),
            code: req.payload.code,
            type: req.payload.type,
            adId: req.payload.adId,
            qty: req.payload.qty,
            freeQty: req.payload.freeQty,
            discount: req.payload.discount,
            price: req.payload.price,
            createdAt: new Date().toString(),
            updatedAt: new Date().toString()
        };
        return models.sequelize.transaction(function (t) {       
            return Promo.create(promo, {transaction: t})          
        })
        .then(() => {
            res({
                message: "Congratulations, Your Promo has been created!",
                data: {}
            }).code(201);
        }).catch(err => {
            res({
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    update(req, res) {
        const updatedPromo = {
            code: req.payload.code,
            type: req.payload.type,
            adId: req.payload.adId,
            qty: req.payload.qty,
            freeQty: req.payload.freeQty,
            discount: req.payload.discount,
            price: req.payload.price,
            updatedAt: new Date().toString()
        }      
        return models.sequelize.transaction(function (t) {
            return Promo.find({
                where: {
                    id: req.payload.id
                }
            })
            .then(Promo => {
                // Check if record exists in db
                if (Promo) {
                    Promo.update(updatedPromo)
                } else {
                    res({
                        message: 'Promo does not exist',
                        data: {}
                    }).code(400);
                }
            });
            
        })
        .then(() => {
            res({
                message: 'You have successful update your Promo',
                data: {}
            }).code(200);
        }).catch(err => {
            res({
                status: 'error',
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    delete(req, res) {
        return models.sequelize.transaction(function (t) {
            return Promo.destroy({where: {id: req.payload.id}, transaction: t})            
        })
        .then(() => {
            res({
                message: "Promo has been deleted!",
                data: {}
            }).code(200);
        }).catch(err => {
            res({
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    // Retrieve Promos By Searching
    searchPromos(req, res) {
        
        let { key } = req.query;
        let offset = 0;
        let pages = 0;
        let dataCount = 0;
        key = (key === '') ? key : `%${key}%`

        return Promo.findAll({
            attributes: ['id', 'code', 'type'],
            where: {
                code: {
                    [Op.iLike]: (key === '') ? '' : key
                }
            },
            include: [
                {
                    attributes: ['name'],
                    model: Ad
                } 
            ]
        })
        .then(promo => {
            const finalPromo = promo.map(x => ({
                id: x.id,
                promoCode: x.code,
                promoType: x.type,
                adName: x.Ad.name
            }))
            res({
                message: '',
                data: finalPromo
            }).code(200)
        })
        .catch(error => res({
            message: error.toString(),
            data: {}
        }).code(400))
    },
};

