import * as Ads from './Ad'
import * as Promos from './Promo'
import * as Customers from './Customer'
import * as Checkouts from './Checkout'

export default {
    Ads,
    Promos,
    Customers,
    Checkouts
}