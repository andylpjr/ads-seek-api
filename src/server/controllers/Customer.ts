import models from '../models/index';
import { generateUuid } from '../../Helper';

const Customer = models.Customer;
const CustomerPromo = models.CustomerPromo;
const Promo = models.Promo;
const Ad = models.Ad;
const Op = models.Sequelize.Op;

export default {
    // Retrieve all Customer
    list(req, res) {
        return Customer
        .findAll({
            attributes: ['id', 'code', 'name'],
        })
        .then(customer => res({
            data: customer
        }).code(200))
        .catch(error => res(error).code(400));
    },
    // Retrieve all Customer
    listById(req, res) {
        return Customer
        .find({
            attributes: ['id', 'code', 'name'],
            where: {
                id: req.query.customerId
            },
            include: [
                {
                    attributes: ['id', 'customerId', 'promoId'],
                    model: CustomerPromo,
                    include: [
                        {
                            attributes: ['code','type', 'adId', 'qty', 'freeQty', 'discount', 'price'],
                            model: Promo,
                            include: [
                                {
                                    attributes: ['code', 'name'],
                                    model: Ad
                                }
                            ]
                        }
                    ]
                }
            ]
        })
        .then(customer => {
            const finalCustomer = {
                id: customer.id,
                code: customer.code,
                name: customer.name,
                detail: customer.CustomerPromos.map(x => ({
                    id: x.id,
                    customerId: x.customerId,
                    promoId: x.promoId,
                    promoCode: x.Promo.code,
                    promoType: x.Promo.type,
                    adName: x.Promo.Ad.name
                }))
            }
            res({
                data: finalCustomer
            }).code(200)
        })
        .catch(error => res(error).code(400));
    },
    // Create Customer
    create(req, res) {
        const customer = {
            id: generateUuid(Customer),
            code: req.payload.code,
            name: req.payload.name,
            price: req.payload.price,
            createdAt: new Date().toString(),
            updatedAt: new Date().toString()
        };
        const customerDetail = req.payload.detail.map(x => ({
            id: generateUuid(CustomerPromo),
            customerId: customer.id,
            promoId: x.promoId
        }))
        return models.sequelize.transaction(function (t) {       
            return Customer.create(customer, {transaction: t})
            .then(() => {
                return CustomerPromo.bulkCreate(customerDetail, {transaction: t})
            })        
        })
        .then(() => {
            res({
                message: "Congratulations, Your Customer has been created!",
                data: {}
            }).code(201);
        }).catch(err => {
            res({
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    update(req, res) {
        const updatedCustomer = {
            code: req.payload.code,
            name: req.payload.name,
            updatedAt: new Date().toString()
        }
        const updatedCustomerPromo = req.payload.detail.map(x => ({
            id: generateUuid(CustomerPromo),
            customerId: req.payload.id,
            promoId: x.promoId
        }))
        return models.sequelize.transaction(function (t) {
            return Customer.find({
                where: {
                    id: req.payload.id
                }
            })
            .then(customer => {
                // Check if record exists in db
                if (customer) {
                    customer.update(updatedCustomer)
                    .then(() => {
                        return CustomerPromo.destroy({
                            where: {
                                customerId: req.payload.id,
                            }
                        })
                        .then(() => {
                            return CustomerPromo.bulkCreate(updatedCustomerPromo)
                        })
                    })
                } else {
                    res({
                        message: 'Customer does not exist',
                        data: {}
                    }).code(400);
                }
            });
            
        })
        .then(() => {
            res({
                message: 'You have successful update your customer',
                data: {}
            }).code(200);
        }).catch(err => {
            res({
                status: 'error',
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    delete(req, res) {
        return models.sequelize.transaction(function (t) {
            return CustomerPromo.destroy({where: { customerId: req.payload.id }, transaction: t})
            .then(() => {
                return Customer.destroy({where: { id: req.payload.id }, transaction: t})
            })            
        })
        .then(() => {
            res({
                message: "Customer has been deleted!",
                data: {}
            }).code(200);
        }).catch(err => {
            res({
                message: err.toString(),
                data: {}
            }).code(400);
        });
    },
    // Retrieve Ads By Searching
    searchCustomers(req, res) {
        
        let { key } = req.query;
        let offset = 0;
        let pages = 0;
        let dataCount = 0;
        key = (key === '') ? key : `%${key}%`

        return Customer.findAll({
            attributes: ['id', 'code', 'name'],
            where: {
                code: {
                    [Op.iLike]: (key === '') ? '' : key
                }
            }
        })
        .then(ad => res({
            message: '',
            data: ad
        }).code(200))
        .catch(error => res({
            message: error.toString(),
            data: {}
        }).code(400))
    },
};

