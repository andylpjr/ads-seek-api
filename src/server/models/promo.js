'use strict';
module.exports = (sequelize, DataTypes) => {
  var Promo = sequelize.define('Promo', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    code: DataTypes.STRING,
    type: DataTypes.STRING,
    adId: DataTypes.UUID,
    qty: DataTypes.INTEGER,
    freeQty: DataTypes.INTEGER,
    discount: DataTypes.DECIMAL(16, 4),
    price: DataTypes.DECIMAL(16, 4),
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    freezeTableName: true,
    tableName: 'Promo'
  });
  Promo.associate = function(models) {
        // associations can be defined here
        Promo.belongsTo(models.Ad, {foreignKey: 'adId', targetKey: 'id'});
        Promo.hasMany(models.CustomerPromo, {foreignKey: 'promoId', sourceKey: 'id'});
        Promo.hasMany(models.CheckoutDetail, {foreignKey: 'promoId', sourceKey: 'id'});
      };
  return Promo;
};