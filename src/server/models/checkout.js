'use strict';
module.exports = (sequelize, DataTypes) => {
  var Checkout = sequelize.define('Checkout', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    documentNo: DataTypes.STRING,
    date: DataTypes.DATEONLY,
    customerId: DataTypes.UUID,
    amount: DataTypes.DECIMAL(16, 4),
    finalAmount: DataTypes.DECIMAL(16, 4),
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    freezeTableName: true,
    tableName: 'Checkout'
  });
  Checkout.associate = function(models) {
        // associations can be defined here
        Checkout.hasMany(models.CheckoutDetail, {foreignKey: 'checkoutId', sourceKey: 'id'});
        Checkout.belongsTo(models.Customer, {foreignKey: 'customerId', targetKey: 'id'});
  };
  return Checkout;
};