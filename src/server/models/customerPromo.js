'use strict';
module.exports = (sequelize, DataTypes) => {
  var CustomerPromo = sequelize.define('CustomerPromo', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    customerId: DataTypes.UUID,
    promoId: DataTypes.UUID,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    freezeTableName: true,
    tableName: 'CustomerPromo'
  });
  CustomerPromo.associate = function(models) {
        // associations can be defined here
        CustomerPromo.belongsTo(models.Customer, {foreignKey: 'customerId', targetKey: 'id'});
        CustomerPromo.belongsTo(models.Promo, {foreignKey: 'promoId', targetKey: 'id'});
  };
  return CustomerPromo;
};