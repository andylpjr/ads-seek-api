'use strict';
module.exports = (sequelize, DataTypes) => {
  var CheckoutDetail = sequelize.define('CheckoutDetail', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    checkoutId: DataTypes.UUID,
    adId: DataTypes.UUID,
    qty: DataTypes.INTEGER,
    price: DataTypes.DECIMAL(16, 4),
    promoId: DataTypes.UUID,
    finalPrice: DataTypes.DECIMAL(16, 4),
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    freezeTableName: true,
    tableName: 'CheckoutDetail'
  });
  CheckoutDetail.associate = function(models) {
        // associations can be defined here
        CheckoutDetail.belongsTo(models.Checkout, {foreignKey: 'checkoutId', targetKey: 'id'});
        CheckoutDetail.belongsTo(models.Ad, {foreignKey: 'adId', targetKey: 'id'});
        CheckoutDetail.belongsTo(models.Promo, {foreignKey: 'promoId', targetKey: 'id'});
      };
  return CheckoutDetail;
};