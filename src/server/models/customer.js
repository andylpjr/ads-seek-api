'use strict';
module.exports = (sequelize, DataTypes) => {
  var Customer = sequelize.define('Customer', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    code: DataTypes.STRING,
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    freezeTableName: true,
    tableName: 'Customer'
  });
  Customer.associate = function(models) {
        // associations can be defined here
        Customer.hasMany(models.CustomerPromo, {foreignKey: 'customerId', sourceKey: 'id'});
        Customer.hasMany(models.Checkout, {foreignKey: 'customerId', sourceKey: 'id'});
  };
  return Customer;
};