'use strict';
module.exports = (sequelize, DataTypes) => {
  var Ad = sequelize.define('Ad', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    code: DataTypes.STRING,
    name: DataTypes.STRING,
    price: DataTypes.DECIMAL(16, 4),
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    freezeTableName: true,
    tableName: 'Ad'
  });
  Ad.associate = function(models) {
        // associations can be defined here
        Ad.hasMany(models.Promo, {foreignKey: 'adId', sourceKey: 'id'});
        Ad.hasMany(models.CheckoutDetail, {foreignKey: 'adId', sourceKey: 'id'});
  };
  return Ad;
};